python-fitsio (1.2.5+dfsg-1) unstable; urgency=medium

  * New upstream version 1.2.5+dfsg

 -- Ole Streicher <olebole@debian.org>  Sat, 08 Feb 2025 14:02:41 +0100

python-fitsio (1.2.4+dfsg-1) unstable; urgency=medium

  * New upstream version 1.2.4+dfsg

 -- Ole Streicher <olebole@debian.org>  Thu, 20 Jun 2024 18:37:28 +0200

python-fitsio (1.2.2+dfsg-1) unstable; urgency=medium

  * New upstream version 1.2.2+dfsg
  * Push Standards-Version to 4.7.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Wed, 12 Jun 2024 19:51:02 +0200

python-fitsio (1.2.1+dfsg-1) unstable; urgency=medium

  * New upstream version 1.2.1+dfsg

 -- Ole Streicher <olebole@debian.org>  Tue, 07 Nov 2023 17:07:17 +0100

python-fitsio (1.2.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.2.0+dfsg
  * Specify --use-system-cfitsio during build and install. Remove patch.

 -- Ole Streicher <olebole@debian.org>  Thu, 03 Aug 2023 18:10:34 +0200

python-fitsio (1.1.10+dfsg-1) unstable; urgency=medium

  * New upstream version 1.1.10+dfsg
  * Drop Mark-failures-that-are-known-to-fail-with-the-original-cf.patch
  * Push Standards-Version to 4.6.2. No changes needed
  * Add new build and test dep py3-pytest

 -- Ole Streicher <olebole@debian.org>  Tue, 27 Jun 2023 17:00:51 +0200

python-fitsio (1.1.8+dfsg-1) unstable; urgency=medium

  * New upstream version 1.1.8+dfsg
  * Drop testCompressPreserveZeros patch: applied upstream

 -- Ole Streicher <olebole@debian.org>  Thu, 29 Sep 2022 14:56:21 +0200

python-fitsio (1.1.7+dfsg-3) unstable; urgency=medium

  * Add missing curl and bz2 dev libs (Closes: #1020027)
  * Push Standards-Version to 4.6.1. No changes needed

 -- Ole Streicher <olebole@debian.org>  Mon, 19 Sep 2022 15:07:35 +0200

python-fitsio (1.1.7+dfsg-2) unstable; urgency=medium

  [ Aurelien Jarno ]
  * testCompressPreserveZeros: do not test with the hcompress algorithm
    Closes: #1011178

 -- Ole Streicher <olebole@debian.org>  Thu, 19 May 2022 07:57:02 +0200

python-fitsio (1.1.7+dfsg-1) unstable; urgency=medium

  * New upstream version 1.1.7+dfsg
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Mon, 31 Jan 2022 12:34:52 +0100

python-fitsio (1.1.6+dfsg-1) unstable; urgency=medium

  * New upstream version 1.1.6+dfsg
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Tue, 21 Dec 2021 13:32:09 +0100

python-fitsio (1.1.5+dfsg-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-Browse.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

  * New upstream version 1.1.5+dfsg
  * Rediff patches
  * Push Standards-Version to 4.6.0. No changes needed
  * Replace deprecated ADTTMP variable with AUTOPKGTEST_TMP in d/t/control

 -- Ole Streicher <olebole@debian.org>  Tue, 14 Sep 2021 09:31:41 +0200

python-fitsio (1.1.4+dfsg-1) unstable; urgency=medium

  * New upstream version 1.1.4+dfsg
  * Rediff patches
  * Push Standards-Version to 4.5.1. No changes needed
  * Add "Rules-Requires-Root: no" to d/control
  * Push dh-compat to 13

 -- Ole Streicher <olebole@debian.org>  Wed, 24 Feb 2021 10:09:50 +0100

python-fitsio (1.1.3+dfsg-2) unstable; urgency=medium

  * Switch back to unstable

 -- Ole Streicher <olebole@debian.org>  Mon, 02 Nov 2020 08:25:06 +0100

python-fitsio (1.1.3+dfsg-1) experimental; urgency=medium

  * New upstream version 1.1.3+dfsg. Rediff patches
  * Try to run build time tests on arm platform. Switch to experimental

 -- Ole Streicher <olebole@debian.org>  Wed, 14 Oct 2020 08:58:47 +0200

python-fitsio (1.1.2+dfsg-1) unstable; urgency=low

  * New upstream version 1.1.2+dfsg. Rediff patches. Closes: #956521

 -- Ole Streicher <olebole@debian.org>  Tue, 28 Apr 2020 08:43:19 +0200

python-fitsio (1.1.1+dfsg-1) unstable; urgency=low

  * New upstream version 1.1.1+dfsg. Rediff patches

 -- Ole Streicher <olebole@debian.org>  Wed, 11 Mar 2020 08:33:41 +0100

python-fitsio (1.1.0+dfsg-1) unstable; urgency=low

  * New upstream version 1.1.0+dfsg. Rediff patches
  * Push Standards-Version to 4.5.0. No changes required

 -- Ole Streicher <olebole@debian.org>  Fri, 21 Feb 2020 17:47:44 +0100

python-fitsio (1.0.5+dfsg-2) unstable; urgency=low

  * Add tests on salsa
  * Remove test for Python-2 package
  * Remove d/compat

 -- Ole Streicher <olebole@debian.org>  Mon, 05 Aug 2019 11:47:37 +0200

python-fitsio (1.0.5+dfsg-1) unstable; urgency=low

  * New upstream version 1.0.5+dfsg. Rediff patches
  * Push Standards-Version to 4.4.0. No changes needed
  * Remove Python 2 package

 -- Ole Streicher <olebole@debian.org>  Thu, 18 Jul 2019 14:08:15 +0200

python-fitsio (0.9.12+dfsg-1) unstable; urgency=low

  * New upstream version 0.9.12+dfsg. Rediff patches
  * Push Standards-Version to 4.3.0. No changes required.
  * New build dependencies setuptools
  * Push compat to 12

 -- Ole Streicher <olebole@debian.org>  Fri, 18 Jan 2019 09:59:47 +0100

python-fitsio (0.9.11+dfsg-4) unstable; urgency=medium

  [ Ole Streicher ]
  * Drop alignment patches. They didn't work.

  [ Matthias Klose ]
  * Dont run autotests on armhf for now (partially resolves #902990)

 -- Ole Streicher <olebole@debian.org>  Sun, 15 Jul 2018 16:36:56 +0200

python-fitsio (0.9.11+dfsg-3) unstable; urgency=medium

  * Disable failing checksum test

 -- Ole Streicher <olebole@debian.org>  Fri, 13 Jul 2018 08:47:51 +0200

python-fitsio (0.9.11+dfsg-2) unstable; urgency=medium

  * Update VCS fields to use salsa.d.o
  * Push compat to 11
  * Push Standards-Version to 4.1.5. Change copyright format URL to https

 -- Ole Streicher <olebole@debian.org>  Thu, 05 Jul 2018 11:09:18 +0200

python-fitsio (0.9.11+dfsg-1) unstable; urgency=low

  * Propagate CI test exit code
  * New upstream version 0.9.11+dfsg
  * Rediff patches
  * Push Standards-Version to 4.0.0. No changes

 -- Ole Streicher <olebole@debian.org>  Fri, 23 Jun 2017 17:44:06 +0200

python-fitsio (0.9.10+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 0.9.10+dfsg

 -- Ole Streicher <olebole@debian.org>  Thu, 18 Aug 2016 14:38:31 +0200

python-fitsio (0.9.9.1+dfsg-1) unstable; urgency=low

  * Imported Upstream version 0.9.9.1+dfsg

 -- Ole Streicher <olebole@debian.org>  Wed, 20 Jul 2016 17:52:17 +0200

python-fitsio (0.9.9+dfsg-1) unstable; urgency=low

  * Imported Upstream version 0.9.9+dfsg

 -- Ole Streicher <olebole@debian.org>  Fri, 15 Jul 2016 08:54:33 +0200

python-fitsio (0.9.8+dfsg-5) unstable; urgency=low

  * Skip bzip2 unittest instead of xfail to avoid bus error on sparc64

 -- Ole Streicher <olebole@debian.org>  Thu, 21 Apr 2016 21:47:58 +0200

python-fitsio (0.9.8+dfsg-4) unstable; urgency=low

  * Align test data in testAsciiTableWriteRead. Closes: #821132

 -- Ole Streicher <olebole@debian.org>  Thu, 21 Apr 2016 14:48:49 +0200

python-fitsio (0.9.8+dfsg-3) unstable; urgency=low

  * Push Standards-Version to 3.9.8 (no changes)
  * Fix FTBFS on mips and powerpc

 -- Ole Streicher <olebole@debian.org>  Wed, 20 Apr 2016 11:22:39 +0200

python-fitsio (0.9.8+dfsg-2) unstable; urgency=low

  * Fix CI test call
  * Fix crash on 32-bit platforms Closes: #821131
  * Fix Vcs-Git URL

 -- Ole Streicher <olebole@debian.org>  Sat, 16 Apr 2016 22:01:34 +0200

python-fitsio (0.9.8+dfsg-1) unstable; urgency=low

  * Initial release. (Closes: #820811)

 -- Ole Streicher <olebole@debian.org>  Thu, 14 Apr 2016 22:56:03 +0200
